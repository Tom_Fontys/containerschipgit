﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Container;

namespace ContainerTest.test
{
    [TestClass]
    public class ShipTest
    {
     
        //[TestMethod]
        //public void TestMethod1()
        //{
        //    List<Container.Container> containers = new List<Container.Container>();
        //    Container.Container container = new Container.Container(24);
        //    Container.Container container1 = new Container.Container(24);
        //    Container.Container container2 = new Container.Container(24);
        //    Container.Container container3 = new Container.Container(24);

        //    Container.Container container4 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container5 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container6 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container7 = new Container.Container(24, Container.Type.Cooled);

        //    containers.Add(container);
        //    containers.Add(container1);
        //    containers.Add(container2);
        //    containers.Add(container3);
        //    containers.Add(container4);
        //    containers.Add(container5);
        //    containers.Add(container6);
        //    containers.Add(container7);
        //    Ship ship = new Ship(4, 3, 840);
        //    Manager man = new Manager(containers, ship);
        //    containers = man.SortContainers();
        //    ship = man.SortedContainerShip(containers);
        //    Assert.AreEqual(2, ship.Rows.Count);
        //}
        [TestMethod]
        public void ShipCreateRowTest()
        {
            int length = 3;
            Ship ship = new Ship(4, length, 840);
            Assert.AreEqual(length, ship.Rows.Count);
        }
        [TestMethod]
        public void ShipCreateNullRowTest()
        {
            int length = 0;
            Ship ship = new Ship(4, length, 840);
            Assert.AreEqual(length, ship.Rows.Count);
        }
        [TestMethod]
        public void ShipAddTest()
        {
            bool Added = true;
            Container.Container container = new Container.Container(26);
            int length = 3;
            Ship ship = new Ship(4, length, 840);
            Assert.AreEqual(Added, ship.Add(container));
        }
        [TestMethod]
        public void ShipAddTestNoRows()
        {
            bool Added = false;
            Container.Container container = new Container.Container(26);
            int length = 0;
            Ship ship = new Ship(4, length, 840);
            Assert.AreEqual(Added, ship.Add(container));
        }
    }    
}
