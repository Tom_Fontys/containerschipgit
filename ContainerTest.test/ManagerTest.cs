﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Container;

namespace ContainerTest.test
{
    [TestClass]
    public class ManagerTest
    {
        [TestMethod]
        public void ManagerSortTestCooled()
        {
            Container.Type Cooled = Container.Type.Cooled;
            Container.Container containerCooled = new Container.Container(0, Container.Type.Cooled);
            Container.Container containerValuable = new Container.Container(0, Container.Type.Important);
            Container.Container containerNormal = new Container.Container(0, Container.Type.Normal);
            List<Container.Container> containers = new List<Container.Container>();
            containers.Add(containerValuable);
            containers.Add(containerNormal);
            containers.Add(containerCooled);
            Ship ship = new Ship(0, 0, 0);
            Manager manager = new Manager(containers, ship);
            containers = manager.SortContainers();
            Container.Container TestContainer = containers[0];
            Container.Type TestType = TestContainer.Type;
            Assert.AreEqual(Cooled, TestType);
        }
        [TestMethod]
        public void ManagerSortTestImportant()
        {
            Container.Type Important = Container.Type.Important;
            Container.Container containerCooled = new Container.Container(0, Container.Type.Cooled);
            Container.Container containerValuable = new Container.Container(0, Container.Type.Important);
            Container.Container containerNormal = new Container.Container(0, Container.Type.Normal);
            List<Container.Container> containers = new List<Container.Container>();
            containers.Add(containerValuable);
            containers.Add(containerNormal);
            containers.Add(containerCooled);
            Ship ship = new Ship(0, 0, 0);
            Manager manager = new Manager(containers, ship);
            containers = manager.SortContainers();
            Container.Container TestContainer = containers[2];
            Container.Type TestType = TestContainer.Type;
            Assert.AreEqual(Important, TestType);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ManagerSortedShip_ToHeavy()
        {
            Container.Container containerCooled = new Container.Container(26, Container.Type.Cooled);
            Container.Container containerValuable = new Container.Container(26, Container.Type.Important);
            Container.Container containerNormal = new Container.Container(26, Container.Type.Normal);
            Container.Container containerNormal1 = new Container.Container(26, Container.Type.Normal);
            List<Container.Container> containers = new List<Container.Container>();
            containers.Add(containerValuable);
            containers.Add(containerNormal);
            containers.Add(containerNormal1);
            containers.Add(containerCooled);
            Ship ship = new Ship(4, 3, 90);
            Manager manager = new Manager(containers, ship);
            ship = manager.SortedContainerShip(containers);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ManagerSortedShip_ToLight()
        {
            Container.Container containerCooled = new Container.Container(26, Container.Type.Cooled);
            Container.Container containerValuable = new Container.Container(26, Container.Type.Important);
            Container.Container containerNormal = new Container.Container(26, Container.Type.Normal);
            Container.Container containerNormal1 = new Container.Container(26, Container.Type.Normal);
            List<Container.Container> containers = new List<Container.Container>();
            containers.Add(containerValuable);
            containers.Add(containerNormal);
            containers.Add(containerNormal1);
            containers.Add(containerCooled);
            Ship ship = new Ship(4, 3, 300);
            Manager manager = new Manager(containers, ship);
            ship = manager.SortedContainerShip(containers);
        }
        [TestMethod]
        public void ManagerSortedShipTest()
        {
            Container.Container containerCooled = new Container.Container(26, Container.Type.Cooled);
            Container.Container containerNormal = new Container.Container(26, Container.Type.Normal);
            List<Container.Container> containers = new List<Container.Container>();
            containers.Add(containerCooled);
            containers.Add(containerNormal);
            containers.Add(containerNormal);
            Ship ship = new Ship(2, 2, 90);
            Manager manager = new Manager(containers, ship);
            ship = manager.SortedContainerShip(containers);
            Row TestRow = ship.Rows[0];
            Stack TestStack = TestRow.Stacks[0];
            Assert.AreEqual(2, TestStack.Containers.Count);
        }
    }    
}
