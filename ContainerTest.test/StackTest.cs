﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Container;

namespace ContainerTest.test
{
    [TestClass]
    public class StackTest
    {

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    List<Container.Container> containers = new List<Container.Container>();
        //    Container.Container container = new Container.Container(24);
        //    Container.Container container1 = new Container.Container(24);
        //    Container.Container container2 = new Container.Container(24);
        //    Container.Container container3 = new Container.Container(24);

        //    Container.Container container4 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container5 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container6 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container7 = new Container.Container(24, Container.Type.Cooled);

        //    containers.Add(container);
        //    containers.Add(container1);
        //    containers.Add(container2);
        //    containers.Add(container3);
        //    containers.Add(container4);
        //    containers.Add(container5);
        //    containers.Add(container6);
        //    containers.Add(container7);
        //    Ship ship = new Ship(4, 3, 840);
        //    Manager man = new Manager(containers, ship);
        //    containers = man.SortContainers();
        //    ship = man.SortedContainerShip(containers);
        //    Assert.AreEqual(2, ship.Rows.Count);
        //}
        [TestMethod]
        public void StackTryToAddTest()
        {
            bool CanAdd = true;
            Stack stack = new Stack(0);
            Container.Container container = new Container.Container(26, Container.Type.Important);
            Assert.AreEqual(CanAdd, stack.TryToAdd(container));
        }
        [TestMethod]
        public void StackTryToAddTest_ContainsValuable()
        {
            bool CanAdd = false;
            Stack stack = new Stack(0);
            Container.Container container = new Container.Container(26, Container.Type.Important);
            stack.Containers.Add(container);
            Assert.AreEqual(CanAdd, stack.TryToAdd(container));
        }
        [TestMethod]
        public void StackTryToAddTest_Contains5HeavyContainers()
        {
            bool CanAdd = false;
            Stack stack = new Stack(0);
            Container.Container container = new Container.Container(26, Container.Type.Important);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            Assert.AreEqual(CanAdd, stack.TryToAdd(container));
        }
    }    
}
