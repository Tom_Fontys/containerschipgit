﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Container;

namespace ContainerTest.test
{
    [TestClass]
    public class RowTest
    {

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    List<Container.Container> containers = new List<Container.Container>();
        //    Container.Container container = new Container.Container(24);
        //    Container.Container container1 = new Container.Container(24);
        //    Container.Container container2 = new Container.Container(24);
        //    Container.Container container3 = new Container.Container(24);

        //    Container.Container container4 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container5 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container6 = new Container.Container(24, Container.Type.Cooled);
        //    Container.Container container7 = new Container.Container(24, Container.Type.Cooled);

        //    containers.Add(container);
        //    containers.Add(container1);
        //    containers.Add(container2);
        //    containers.Add(container3);
        //    containers.Add(container4);
        //    containers.Add(container5);
        //    containers.Add(container6);
        //    containers.Add(container7);
        //    Ship ship = new Ship(4, 3, 840);
        //    Manager man = new Manager(containers, ship);
        //    containers = man.SortContainers();
        //    ship = man.SortedContainerShip(containers);
        //    Assert.AreEqual(2, ship.Rows.Count);
        //}
        [TestMethod]
        public void RowTryToAdd_CooledNotFirst()
        {
            bool CanAdd = false;
            Row row = new Row(2);
            row.isFirst = false;
            Container.Container container = new Container.Container(26, Container.Type.Cooled);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }
        [TestMethod]
        public void RowTryToAdd_CooledFirst()
        {
            bool CanAdd = true;
            Row row = new Row(2);
            row.isFirst = true;
            Container.Container container = new Container.Container(26, Container.Type.Cooled);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }
        [TestMethod]
        public void RowTryToAdd_ValuableNotFirst()
        {
            bool CanAdd = false;
            Row row = new Row(2);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Important);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }
        [TestMethod]
        public void RowTryToAdd_ValuableFirst()
        {
            bool CanAdd = true;
            Row row = new Row(2);
            row.isFirst = true;
            Container.Container container = new Container.Container(26, Container.Type.Important);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }
        [TestMethod]
        public void RowTryToAdd_ValuableLast()
        {
            bool CanAdd = true;
            Row row = new Row(2);
            row.isFirst = false;
            row.isLast = true;
            Container.Container container = new Container.Container(26, Container.Type.Important);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }
        [TestMethod]
        public void RowTryToAdd_0Stacks()
        {
            bool CanAdd = false;
            Row row = new Row(0);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Important);
            Assert.AreEqual(CanAdd, row.TryToAdd(container));
        }


        [TestMethod]
        public void Row_3_TryToAdd_1ststackfull()
        {
            bool CanAdd = true;
            Row row = new Row(3);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Normal);
            Stack stack = new Stack(0);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            row.Stacks[0] = stack;
            row.TryToAdd(container);
            Stack TestStack = row.Stacks[2];
            Assert.AreEqual(1, TestStack.Containers.Count);
        }
        [TestMethod]
        public void Row_3_TryToAdd_1ststackheavy()
        {
            bool CanAdd = true;
            Row row = new Row(3);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Normal);
            Stack stack = new Stack(0);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            row.Stacks[0] = stack;
            row.TryToAdd(container);
            Stack TestStack = row.Stacks[2];
            Assert.AreEqual(1, TestStack.Containers.Count);
        }
        [TestMethod]
        public void Row_3_TryToAdd_stack1and3full()
        {
            bool CanAdd = true;
            Row row = new Row(3);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Normal);
            Stack stack = new Stack(0);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            Stack stack2 = new Stack(0);
            stack2.Containers.Add(container);
            stack2.Containers.Add(container);
            stack2.Containers.Add(container);
            stack2.Containers.Add(container);
            stack2.Containers.Add(container);
            row.Stacks[0] = stack;
            row.Stacks[2] = stack;
            row.TryToAdd(container);
            Stack TestStack = row.Stacks[1];
            Assert.AreEqual(1, TestStack.Containers.Count);
        }
        [TestMethod]
        public void Row_4_TryToAdd_stack1full()
        {
            bool CanAdd = true;
            Row row = new Row(4);
            row.isFirst = false;
            row.isLast = false;
            Container.Container container = new Container.Container(26, Container.Type.Normal);
            Stack stack = new Stack(0);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            stack.Containers.Add(container);
            row.Stacks[0] = stack;
            row.TryToAdd(container);
            Stack TestStack = row.Stacks[2];
            Assert.AreEqual(1, TestStack.Containers.Count);
        }
    }    
}
