﻿namespace Container
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Width = new System.Windows.Forms.TextBox();
            this.Length = new System.Windows.Forms.TextBox();
            this.ShipWeight = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CreateShipBtn = new System.Windows.Forms.Button();
            this.Debug = new System.Windows.Forms.Label();
            this.AddContainer = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ContainerTypes = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ContainerWeight = new System.Windows.Forms.TextBox();
            this.AllContainers = new System.Windows.Forms.ListBox();
            this.StackInfo = new System.Windows.Forms.ListBox();
            this.PlaceContainers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Width
            // 
            this.Width.Location = new System.Drawing.Point(83, 25);
            this.Width.Name = "Width";
            this.Width.Size = new System.Drawing.Size(100, 22);
            this.Width.TabIndex = 0;
            this.Width.Text = "4";
            // 
            // Length
            // 
            this.Length.Location = new System.Drawing.Point(83, 53);
            this.Length.Name = "Length";
            this.Length.Size = new System.Drawing.Size(100, 22);
            this.Length.TabIndex = 1;
            this.Length.Text = "3";
            // 
            // ShipWeight
            // 
            this.ShipWeight.Location = new System.Drawing.Point(83, 81);
            this.ShipWeight.Name = "ShipWeight";
            this.ShipWeight.Size = new System.Drawing.Size(100, 22);
            this.ShipWeight.TabIndex = 2;
            this.ShipWeight.Text = "840";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "MaxWeight";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Length";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Width";
            // 
            // CreateShipBtn
            // 
            this.CreateShipBtn.Location = new System.Drawing.Point(36, 109);
            this.CreateShipBtn.Name = "CreateShipBtn";
            this.CreateShipBtn.Size = new System.Drawing.Size(147, 23);
            this.CreateShipBtn.TabIndex = 6;
            this.CreateShipBtn.Text = "Save";
            this.CreateShipBtn.UseVisualStyleBackColor = true;
            this.CreateShipBtn.Click += new System.EventHandler(this.CreateShipBtn_Click);
            // 
            // Debug
            // 
            this.Debug.AutoSize = true;
            this.Debug.Location = new System.Drawing.Point(12, 522);
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(77, 17);
            this.Debug.TabIndex = 7;
            this.Debug.Text = "MaxWeight";
            // 
            // AddContainer
            // 
            this.AddContainer.Location = new System.Drawing.Point(86, 260);
            this.AddContainer.Name = "AddContainer";
            this.AddContainer.Size = new System.Drawing.Size(109, 23);
            this.AddContainer.TabIndex = 8;
            this.AddContainer.Text = "Add Container";
            this.AddContainer.UseVisualStyleBackColor = true;
            this.AddContainer.Click += new System.EventHandler(this.AddContainer_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Weight + 4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Type";
            // 
            // ContainerTypes
            // 
            this.ContainerTypes.FormattingEnabled = true;
            this.ContainerTypes.Location = new System.Drawing.Point(74, 218);
            this.ContainerTypes.Name = "ContainerTypes";
            this.ContainerTypes.Size = new System.Drawing.Size(121, 24);
            this.ContainerTypes.TabIndex = 11;
            // 
            // ContainerWeight
            // 
            this.ContainerWeight.Location = new System.Drawing.Point(95, 188);
            this.ContainerWeight.Name = "ContainerWeight";
            this.ContainerWeight.Size = new System.Drawing.Size(100, 22);
            this.ContainerWeight.TabIndex = 12;
            this.ContainerWeight.Text = "26";
            // 
            // AllContainers
            // 
            this.AllContainers.FormattingEnabled = true;
            this.AllContainers.ItemHeight = 16;
            this.AllContainers.Location = new System.Drawing.Point(3, 289);
            this.AllContainers.Name = "AllContainers";
            this.AllContainers.Size = new System.Drawing.Size(252, 228);
            this.AllContainers.TabIndex = 13;
            // 
            // StackInfo
            // 
            this.StackInfo.FormattingEnabled = true;
            this.StackInfo.ItemHeight = 16;
            this.StackInfo.Location = new System.Drawing.Point(1060, 14);
            this.StackInfo.Name = "StackInfo";
            this.StackInfo.Size = new System.Drawing.Size(265, 164);
            this.StackInfo.TabIndex = 14;
            // 
            // PlaceContainers
            // 
            this.PlaceContainers.Location = new System.Drawing.Point(235, 24);
            this.PlaceContainers.Name = "PlaceContainers";
            this.PlaceContainers.Size = new System.Drawing.Size(75, 23);
            this.PlaceContainers.TabIndex = 15;
            this.PlaceContainers.Text = "Place";
            this.PlaceContainers.UseVisualStyleBackColor = true;
            this.PlaceContainers.Click += new System.EventHandler(this.PlaceContainers_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1337, 548);
            this.Controls.Add(this.PlaceContainers);
            this.Controls.Add(this.StackInfo);
            this.Controls.Add(this.AllContainers);
            this.Controls.Add(this.ContainerWeight);
            this.Controls.Add(this.ContainerTypes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AddContainer);
            this.Controls.Add(this.Debug);
            this.Controls.Add(this.CreateShipBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ShipWeight);
            this.Controls.Add(this.Length);
            this.Controls.Add(this.Width);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Width;
        private System.Windows.Forms.TextBox Length;
        private System.Windows.Forms.TextBox ShipWeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CreateShipBtn;
        private System.Windows.Forms.Label Debug;
        private System.Windows.Forms.Button AddContainer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ContainerTypes;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox ContainerWeight;
        private System.Windows.Forms.ListBox AllContainers;
        private System.Windows.Forms.ListBox StackInfo;
        private System.Windows.Forms.Button PlaceContainers;
    }
}

