﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public class Stack
    {
        int maxStackWeight = 120;
        public int TotalWeight => Containers.Sum(o => o.Weight);
        public int Id { get;}
        public List<Container> Containers { get; set; }
        public Stack(int id)
        {
            Id = id;
            Containers = new List<Container>();
        }
        private bool ToMuchWeight(int Addweight)
        {
            for (int i = 0; i < Containers.Count; i++)
            {
                int Weight = 0;
                for (int o = i + 1; o < Containers.Count; o++)
                {
                    Weight += Containers[o].Weight;
                }
                if (Weight + Addweight > maxStackWeight)
                    return true;
            }
            return false;
        }
        private bool ContainsValuable()
        {
            foreach (Container container in Containers)
            {
                var valuable = (container.Type == Type.Important);
                if (valuable)
                {
                    return true;
                }
            }
            return false;
        }
        public bool TryToAdd(Container container)
        {
            if (ToMuchWeight(container.Weight)) return false;
            if (ContainsValuable()) return false;
            Containers.Add(container);
            return true;
        }
    }
}
