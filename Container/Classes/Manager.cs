﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public class Manager
    {
        private List<Container> CreatedContainers;
        private Ship CreatedShip;
        public Manager(List<Container> GivenContainers, Ship GivenShip)
        {
            CreatedContainers = GivenContainers;
            CreatedShip = GivenShip;
        }
        private bool CheckContainerWeightToLight(List<Container> containers)
        {
            int TotalWeight = containers.Sum(o => o.Weight);
            int MinimumWeight = CreatedShip.MaxWeight / 2;
            if (TotalWeight < MinimumWeight)
            {
                return true;
            }
            return false;
        }
        private bool CheckContainerWeightToHeavy(List<Container> containers)
        {
            int TotalWeight = containers.Sum(o => o.Weight);
            int MaxWeight = CreatedShip.MaxWeight;
            if (TotalWeight > MaxWeight)
            {
                return true;
            }
            return false;
        }
        public Ship SortedContainerShip(List<Container> containers)
        {
            if(CheckContainerWeightToLight(containers))
                throw new ArgumentException("container weigh too little.");
            if (CheckContainerWeightToHeavy(containers))
                throw new ArgumentException("container weigh too much.");
            foreach (Container container in containers.ToList())
            {
                if(CreatedShip.Add(container)) containers.Remove(container);
            }
            return CreatedShip;
        }
        public List<Container> SortContainers()
        {
            List<Container> SortedList = new List<Container>();
            List<Container> CooledList = new List<Container>();
            List<Container> ValuableList = new List<Container>();

            CooledList = Sorted(Type.Cooled, CooledList);
            ValuableList = Sorted(Type.Important, ValuableList);

            CreatedContainers.Sort((x, b) => b.Weight.CompareTo(x.Weight));
            ValuableList.Sort((x, b) => b.Weight.CompareTo(x.Weight));
            CooledList.Sort((x, b) => b.Weight.CompareTo(x.Weight));

            SortedList.AddRange(CooledList);
            SortedList.AddRange(CreatedContainers);
            SortedList.AddRange(ValuableList);
            return SortedList;
        }
        private List<Container> Sorted(Type type, List<Container> ToSortTo)
        {
            foreach (Container container in CreatedContainers.ToList())
            {
                var canAdd = (container.Type == type);
                if (canAdd)
                {
                    ToSortTo.Add(container);
                    CreatedContainers.Remove(container);
                }
            }
            return ToSortTo;
        }
    }
}
