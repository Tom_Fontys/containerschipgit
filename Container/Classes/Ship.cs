﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public class Ship
    {
        public List<Row> Rows { get; set; }
        public int Width { get; private set; }
        public int Length { get; private set; }
        public int MaxWeight { get; }
        public Ship(int width, int length, int maxWeight)
        {
            Width = width;
            Length = length;
            MaxWeight = maxWeight;
            Rows = new List<Row>();

            for (int i = 0; i < length; i++)
            {
                bool isFirst = (i == 0);
                bool isLast = (i == length - 1);

                Rows.Add(new Row(width, isFirst, isLast));
            }
        }
        public bool Add(Container toAdd)
        {
            foreach (var row in Rows)
            {
                if (row.TryToAdd(toAdd)) return true;
            }
            return false;
        }


    }
}
