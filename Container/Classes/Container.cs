﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public enum Type { Cooled, Important, Normal }
    public class Container
    {
        int maxWeight = 30;
        int emptyWeight = 4;
        private Type type;
        public int Weight { get; set; }
        public Type Type { get { return this.type; } }
        public Container(int weight, Type type = Type.Normal)
        {
            Weight = emptyWeight + weight;
            this.type = type;
        }
        override public string ToString()
        {
            return type.ToString() + " " + "Container. Weight:" + Weight;
        }
    }
}
