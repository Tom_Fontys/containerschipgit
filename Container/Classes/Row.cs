﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container
{
    public class Row
    {
        static int GiveID = 0;
        public List<Stack> Stacks { get; set; }
        public bool isFirst = false;
        public bool isLast = false;
        public Row(int width, bool isFirst = false, bool isLast = false)
        {
            this.isFirst = isFirst;
            this.isLast = isLast;

            Stacks = new List<Stack>();
            for (int i = 0; i < width; i++)
            {
                Stacks.Add(new Stack(GiveID));
                GiveID++;
            }
        }
        public bool TryToAdd(Container container)
        {
            if (container.Type == Type.Cooled && !isFirst) return false;
            if (container.Type == Type.Important && !isFirst && !isLast) return false;
            if(WeightLeftSide() > WeightRightSide())
            {
                var startIndex = (Stacks.Count % 2 == 0) ? (Stacks.Count / 2) : (Stacks.Count + 1) / 2;
                for (int i = startIndex; i < Stacks.Count; i++)
                {
                    if (Stacks[i].TryToAdd(container)) return true; ;
                }
            }
            else
            {
                var startIndex = 0;
                var width = (Stacks.Count % 2 == 0) ? (Stacks.Count / 2) : (Stacks.Count + 1) / 2;
                for (int i = startIndex; i < width; i++)
                {
                    if (Stacks[i].TryToAdd(container)) return true;
                }
            }
            return false;
        }
        private int WeightLeftSide()
        {
            int weight = 0;
            var width = (Stacks.Count % 2 == 0) ? (Stacks.Count / 2) : (Stacks.Count + 1) / 2;
            for (int i = 0; i < width; i++)
            {
                weight += Stacks[i].TotalWeight;
            }
            return weight;
        }
        private int WeightRightSide()
        {
            int weight = 0;
            var startIndex = (Stacks.Count % 2 == 0) ? (Stacks.Count / 2) : (Stacks.Count + 1) / 2;
            for (int i = startIndex; i < Stacks.Count; i++)
            {
                weight += Stacks[i].TotalWeight;
            }
            return weight;
        }
    }
}
