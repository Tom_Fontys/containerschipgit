﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Container
{
    public partial class Form1 : Form
    {
        Ship ship;
        List<Container> ContainersToAdd;
        Manager manager;
        public Form1()
        {
            InitializeComponent();
            ContainersToAdd = new List<Container>();
            ContainerTypes.Items.Add("Valuable");
            ContainerTypes.Items.Add("Cooled");
            ContainerTypes.Items.Add("Normal");
            AddContainer.Enabled = false;
            CreateShipBtn.Enabled = true;
            PlaceContainers.Enabled = false;
        }

        private void button_click(object sender, EventArgs e)
        {
            StackInfo.Items.Clear();
            Button btn = sender as Button;
            int id = Convert.ToInt32(btn.Text);
            foreach (var row in ship.Rows)
            {
                foreach (var stack in row.Stacks)
                {
                    if (stack.Id == id)
                    {
                        MessageBox.Show(stack.Id.ToString() + " "+ btn.Text);
                        foreach (var item in stack.Containers)
                        {
                            StackInfo.Items.Add(item);
                        }
                        //string s = item.Containers[0].Weight.ToString();
                    }
                }
            }
        }

        private void CreateShipBtn_Click(object sender, EventArgs e)
        {
            List<int> names = new List<int>();
            int Weight = Convert.ToInt32(ShipWeight.Text);
            int ShipWidth = Convert.ToInt32(Width.Text);
            int ShipLength = Convert.ToInt32(Length.Text);
            ship = new Ship(ShipWidth, ShipLength, Weight);
            int lenght = ShipWidth * ShipLength;
            for (int i = 0; i < lenght; i++)
            {
                names.Add(i);
            }
            for (int i = 0; i < ShipLength; i++)
            {
                int y = 200 + i * 30;
                for (int s = 0; s < ShipWidth; s++)
                {
                    string name = names[0].ToString();
                    int x = 200 + s * 100;
                    Button newButton = new Button()
                    {
                        Parent = this,                // place new button on the current form
                        Location = new Point(x, y), // place at x = 10, y = 20
                        Text = name,          // Text on the button
                        //TODO: Add whatever button parameters you want 
                    };
                    newButton.Click += new EventHandler(this.button_click);
                    names.RemoveAt(0);
                }
            }
            CreateShipBtn.Enabled = false;
            AddContainer.Enabled = true;
        }
        private bool SumContainerWeightToHeavy(Container container)
        {
            if (ContainersToAdd.Sum(o => o.Weight) + container.Weight > ship.MaxWeight)
                return true;
            return false;
        }
        private bool SumContainerWeightToLight(Container container)
        {
            if (ContainersToAdd.Sum(o => o.Weight) + container.Weight < ship.MaxWeight / 2)
                return true;
            return false;
        }

        private void AddContainer_Click(object sender, EventArgs e)
        {
            Container container;
            int weight = Convert.ToInt32(ContainerWeight.Text);
            switch (ContainerTypes.Text)
            {
                case "Normal":
                    container = new Container(weight);
                    break;

                case "Valuable":
                    container = new Container(weight, Type.Important);
                    break;

                case "Cooled":
                    container = new Container(weight, Type.Cooled);
                    break;

                default:
                    container = new Container(weight);
                    break;
            }
            if (!SumContainerWeightToHeavy(container))
            {
                if (!SumContainerWeightToLight(container))
                    PlaceContainers.Enabled = true;
                ContainersToAdd.Add(container);
                ShowAllContainers();
                Debug.Text = ContainersToAdd.Sum(o => o.Weight).ToString();
            }
            else
            {
                MessageBox.Show("The containers would weigh more than the ship!");
            }
        }
        private void ShowAllContainers()
        {
            manager = new Manager(ContainersToAdd, ship);
            ContainersToAdd = manager.SortContainers();
            AllContainers.Items.Clear();
            foreach (var item in ContainersToAdd)
            {
                AllContainers.Items.Add(item);
            }
        }

        private void PlaceContainers_Click(object sender, EventArgs e)
        {
            ship = manager.SortedContainerShip(ContainersToAdd);
            DisplayLeftOvers();
        }
        private void DisplayLeftOvers()
        {
            List<Container> LeftOvers = new List<Container>();
            foreach (var Row in ship.Rows)
            {
                foreach (var stack in Row.Stacks)
                {
                    LeftOvers.AddRange(stack.Containers);
                }
            }
            LeftOvers = ContainersToAdd.Except(LeftOvers).ToList();
            AllContainers.Items.Clear();
            foreach (var item in LeftOvers)
            {
                AllContainers.Items.Add(item);
            }
        }
    }
}
